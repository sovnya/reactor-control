local energy = require("lib.energy")
local whttp = require("lib.http")

local config = require("config")

local reactors = energy.powah.generator.reactor:get_peripherals()
local magmators = energy.powah.generator.magmator:get_peripherals()
local stuffz = {}
local things = {}

if config.reporting_enabled then
    things[#things + 1] = function()
        while true do
            for _, v in pairs(reactors) do
                stuffz[#stuffz + 1] = function()
                    local _, machine_type, id = energy.powah.get_machine_type(v)
                    local info = energy.powah.generator.reactor.inspect(v)
                    ---@type BasicReactor
                    local body = {
                        id = id,
                        machine_type = machine_type,
                        name = v,
                        info = info,
                    }
                    local request_body = whttp.reactor.make_body(body)
                    --print(textutils.serialiseJSON(request_body.materials))
                    whttp.reactor.send_request(config, request_body)
                end
            end
            parallel.waitForAll(table.unpack(stuffz))
            sleep(2.5)
            stuffz = {}
        end
    end

    things[#things+1] = function ()
        while true do
            for _, v in pairs(magmators) do
                stuffz[#stuffz + 1] = function()
                    local _, machine_type, id = energy.powah.get_machine_type(v)
                    local wrap = peripheral.wrap(v)
                    local body = {
                        name = v,
                        id = id,
                        machine_type = machine_type,
                        tanks = wrap.tanks(),
                    }

                    local request_body = whttp.magmator.make_body(body)
                    whttp.magmator.send_request(config, request_body)
                end
            end

            parallel.waitForAll(table.unpack(stuffz))
            sleep(2.5)
            stuffz = {}
        end
    end
end

parallel.waitForAll(table.unpack(things))