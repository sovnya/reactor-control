return {
    url = "http://127.0.0.1:8080/",
    reporting_enabled = true,
    monitor = "top",
    reactors_auto_detect = true,
    reactors = {}
}