local energy = require("lib.energy")
local floor = math.floor

return {
    make_body = function(magmator)
        require("cc.pretty").pretty_print(magmator)
        return {
            name = magmator.name,
            id = magmator.id,
            machine_type = magmator.machine_type,
            energy = {
                production = floor(energy.get_generation(magmator.name)),
                capacity = floor(energy.get_capacity(magmator.name)),
                stored = floor(energy.get_stored(magmator.name)),
            },
            tanks = magmator.tanks,
        }
    end,
    send_request = function(config, body)
        local url = config.url .. "api/magmator/" .. body.id
        local request, what = http.post(url, textutils.serialiseJSON(body), {
            ["Content-Type"] = "application/json",
        })
        print("Sending Magmator request for: " .. body.id)
    end,
}
