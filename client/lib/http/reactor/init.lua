local energy = require("lib.energy")

local floor = math.floor

---@class Reactor
---@field name string
---@field id number
---@field machine_type ReactorType
---@field energy ReactorEnergy
---@field temperature ReactorTemperature
---@field materials ReactorMaterials

---@class ReactorEnergy
---@field production number
---@field capacity number
---@field stored number

---@class ReactorTemperature
---@field temperature number
---@field max_temperature number
---@field coolant string

---@class ReactorMaterials
---@field redstone number
---@field max_redstone number
---@field carbon number
---@field max_carbon number
---@field coolant number
---@field max_coolant number
---@field uranium number
---@field max_uranium number
---@field uranium_consumption number

---@class BasicReactor
---@field name string
---@field id number
---@field machine_type ReactorType
---@field info ReactorInspect

---@enum ReactorType
local ReactorType = {
    Starter = "starter",
    Basic = "basic",
    Hardened = "hardened",
    Blazing = "blazing",
    Niotic = "niotic",
    Spirited = "spirited",
    Nitro = "nitro",
}

return {
    ---@param reactor BasicReactor
    make_body = function(reactor)
        ---@type Reactor
        local reactor_body = {
            name = reactor.name,
            id = reactor.id,
            machine_type = reactor.machine_type,
            energy = {
                production = floor(reactor.info.energyProduction),
                capacity = floor(energy.get_capacity(reactor.name)),
                stored = floor(energy.get_stored(reactor.name)),
            },
            temperature = {
                temperature = floor(reactor.info.currentTemp),
                max_temperature = floor(reactor.info.maxTemp),
                coolant = "powah:dry_ice",
            },
            materials = {
                redstone = floor(reactor.info.currentRedstone),
                max_redstone = floor(reactor.info.maxRedstone),

                carbon = floor(reactor.info.currentCarbon),
                max_carbon = floor(reactor.info.maxCarbon),

                coolant = floor(reactor.info.currentSolidCoolant),
                max_coolant = floor(reactor.info.maxSolidCoolant),

                uranium = floor(reactor.info.currentUranium),
                max_uranium = floor(reactor.info.maxUranium),
                uranium_consumption = floor(reactor.info.uraniumConsumption),
            },
        }

        return reactor_body
    end,
    send_request = function(config, body)
        local url = config.url .. "api/reactor/" .. body.id
        local request, what = http.post(url, textutils.serialiseJSON(body), {
            ["Content-Type"] = "application/json",
        })
        print("Sending Reactor request for: " .. body.id)
    end,
}
