local old_path = package.path
local sourcePath = fs.getDir(select(2, ...) or "")
package.path = string.format(package.path .. "/%s/?;/%s/?.lua;/%s/?/init.lua;", sourcePath, sourcePath, sourcePath)

local whttp = {
    reactor = require("reactor"),
    magmator = require("magmator"),
}

package.path = old_path

return whttp
