use axum::{
    extract::{Path, State},
    http::StatusCode,
    response::IntoResponse,
    routing::{get, post},
    Json, Router,
};

use crate::{json::structures::magmator::Magmator, SharedState};

pub fn routes() -> Router<SharedState> {
    Router::new()
        .route("/", get(magmator_list))
        .route("/:id", get(magmator_information))
        .route("/:id", post(update_magmator_information))
}

async fn magmator_list(State(state): State<SharedState>) -> impl IntoResponse {
    let magmators: Vec<Magmator> = state
        .read()
        .unwrap()
        .magmators
        .iter()
        .map(|(_, b)| b.clone())
        .collect();

    Json(magmators)
}

async fn magmator_information(
    State(state): State<SharedState>,
    Path(id): Path<i32>,
) -> impl IntoResponse {
    if let Some(magmator) = state.read().unwrap().magmators.get(&id) {
        return Json(magmator).into_response();
    }

    (StatusCode::NOT_FOUND, "Ay mate not found").into_response()
}

async fn update_magmator_information(
    State(state): State<SharedState>,
    Path(id): Path<i32>,
    Json(payload): Json<Magmator>,
) -> impl IntoResponse {
    println!("id={id}; {:#?}", payload);

    state
        .write()
        .unwrap()
        .magmators
        .entry(id)
        .and_modify(|e| *e = payload.clone())
        .or_insert_with(|| payload.clone());

    "OK"
}
