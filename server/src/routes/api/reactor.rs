use axum::{
    extract::{Path, State},
    http::StatusCode,
    response::IntoResponse,
    routing::{get, post},
    Json, Router,
};

use crate::{json::structures::reactor::Reactor, SharedState};

pub fn routes() -> Router<SharedState> {
    Router::new()
        .route("/", get(reactor_list))
        .route("/:id", get(reactor_information))
        .route("/:id", post(update_reactor_information))
}

async fn reactor_list(State(state): State<SharedState>) -> impl IntoResponse {
    let reactors: Vec<Reactor> = state
        .read()
        .unwrap()
        .reactors
        .iter()
        .map(|(_, b)| b.clone())
        .collect();

    Json(reactors)
}

async fn reactor_information(
    State(state): State<SharedState>,
    Path(id): Path<i32>,
) -> impl IntoResponse {
    if let Some(reactor) = state.read().unwrap().reactors.get(&id) {
        return Json(reactor).into_response();
    }

    (StatusCode::NOT_FOUND, "Ay mate not found").into_response()
}

async fn update_reactor_information(
    State(state): State<SharedState>,
    Path(id): Path<i32>,
    Json(payload): Json<Reactor>,
) -> impl IntoResponse {
    println!("id={id}; {:#?}", payload);

    state
        .write()
        .unwrap()
        .reactors
        .entry(id)
        .and_modify(|e| *e = payload.clone())
        .or_insert_with(|| payload.clone());

    "OK"
}
