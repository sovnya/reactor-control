use axum::Router;

use crate::SharedState;

pub mod magmator;
pub mod reactor;

pub fn routes() -> Router<SharedState> {
    Router::new()
        .nest("/reactor", reactor::routes())
        .nest("/magmator", magmator::routes())
}
