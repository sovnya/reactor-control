use axum::{
    extract::{Path, State},
    http::StatusCode,
    response::{Html, IntoResponse},
    routing::get,
    Router,
};

use crate::{
    json::structures::{magmator::Magmator, reactor::Reactor},
    SharedState,
};

pub mod api;

pub fn routes() -> Router<SharedState> {
    Router::new()
        .route("/", get(index))
        .route("/reactor/:id", get(reactor_page))
        .route("/magmator", get(magmators_page))
        .route("/magmator/:id", get(magmator_page))
        .nest("/api", api::routes())
}

async fn index(State(state): State<SharedState>) -> impl IntoResponse {
    let templates = &state.read().unwrap().templates;
    let reactors: Vec<Reactor> = state
        .read()
        .unwrap()
        .reactors
        .iter()
        .map(|(_, b)| b.clone())
        .collect();

    let mut ctx = tera::Context::new();
    ctx.insert("reactors", &reactors);

    let render = templates
        .render("index.html.tera", &ctx)
        .map_err(|err| {
            tracing::error!(error = ?err, "Something went the fuck wrong");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Something went the fuck wrong: {err}"),
            )
                .into_response()
        })
        .unwrap();

    Html(render)
}

async fn reactor_page(State(state): State<SharedState>, Path(id): Path<i32>) -> impl IntoResponse {
    let state = state.read().unwrap();
    let templates = &state.templates;
    let reactor = state.reactors.get(&id);

    let mut ctx = tera::Context::new();

    // Really stupid but i guess it works
    if let Some(woop) = reactor {
        ctx.insert("reactor", woop);
    }

    let render = templates
        .render("reactor/reactor.html.tera", &ctx)
        .map_err(|err| {
            tracing::error!(error = ?err, "Something went the fuck wrong");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Something went the fuck wrong: {err}"),
            )
                .into_response()
        })
        .unwrap();

    Html(render)
}

async fn magmator_page(State(state): State<SharedState>, Path(id): Path<i32>) -> impl IntoResponse {
    let state = state.read().unwrap();
    let templates = &state.templates;
    let magmator = state.magmators.get(&id);

    let mut ctx = tera::Context::new();

    // Really stupid but i guess it works
    if let Some(woop) = magmator {
        ctx.insert("magmator", woop);
    }

    let render = templates
        .render("magmator/magmator.html.tera", &ctx)
        .map_err(|err| {
            tracing::error!(error = ?err, "Something went the fuck wrong");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Something went the fuck wrong: {err}"),
            )
                .into_response()
        })
        .unwrap();

    Html(render)
}

async fn magmators_page(State(state): State<SharedState>) -> impl IntoResponse {
    let state = state.read().unwrap();
    let templates = &state.templates;
    let magmators: Vec<Magmator> = state.magmators.iter().map(|(_, b)| b.clone()).collect();

    let mut ctx = tera::Context::new();
    ctx.insert("magmators", &magmators);

    let render = templates
        .render("magmator/magmators.html.tera", &ctx)
        .map_err(|err| {
            tracing::error!(error = ?err, "Something went the fuck wrong");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Something went the fuck wrong: {err}"),
            )
                .into_response()
        })
        .unwrap();

    Html(render)
}
