use serde::{Deserialize, Serialize};

use super::{Energy, MachineType, Tank};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Magmator {
    pub name: String,
    pub id: i64,
    pub tanks: Vec<Tank>,
    pub energy: Energy,
    pub machine_type: MachineType,
}
