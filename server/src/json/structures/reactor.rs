use serde::{Deserialize, Serialize};

use super::{Energy, MachineType};

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Reactor {
    pub name: String,
    pub id: i32,
    pub machine_type: MachineType,
    pub energy: Energy,
    pub temperature: ReactorTemperature,
    pub materials: ReactorMaterials,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct ReactorTemperature {
    pub temperature: i32,
    pub max_temperature: i32,
    pub coolant: String,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct ReactorMaterials {
    pub redstone: i32,
    pub max_redstone: i32,

    pub carbon: i32,
    pub max_carbon: i32,

    pub coolant: i32,
    pub max_coolant: i32,

    pub uranium: i32,
    pub max_uranium: i32,
    pub uranium_consumption: i32,
}

impl Default for Reactor {
    fn default() -> Self {
        Self {
            name: Default::default(),
            id: Default::default(),
            machine_type: Default::default(),
            energy: Default::default(),
            temperature: Default::default(),
            materials: Default::default(),
        }
    }
}

impl Default for ReactorTemperature {
    fn default() -> Self {
        Self {
            temperature: Default::default(),
            max_temperature: Default::default(),
            coolant: Default::default(),
        }
    }
}

impl Default for ReactorMaterials {
    fn default() -> Self {
        Self {
            redstone: Default::default(),
            max_redstone: Default::default(),
            carbon: Default::default(),
            max_carbon: Default::default(),
            coolant: Default::default(),
            max_coolant: Default::default(),
            uranium: Default::default(),
            max_uranium: Default::default(),
            uranium_consumption: Default::default(),
        }
    }
}
