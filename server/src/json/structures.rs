use serde::{Deserialize, Serialize};

pub mod magmator;
pub mod reactor;

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Energy {
    pub production: u32,
    pub capacity: u32,
    pub stored: u32,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Tank {
    pub amount: i32,
    pub name: String,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
#[serde(rename_all = "lowercase")]
pub enum MachineType {
    Unknown,
    Starter,
    Basic,
    Hardened,
    Blazing,
    Niotic,
    Spirited,
    Nitro,
}

impl Default for MachineType {
    fn default() -> Self {
        MachineType::Unknown
    }
}

impl Default for Energy {
    fn default() -> Self {
        Self {
            production: 0,
            capacity: 0,
            stored: 0,
        }
    }
}

impl Default for Tank {
    fn default() -> Self {
        Self {
            amount: 0,
            name: "minecraft:water".into(),
        }
    }
}
