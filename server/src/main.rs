use std::sync::{Arc, RwLock};

use hashbrown::HashMap;

use axum::{http::Method, Router};
use tower_http::{
    cors::{Any, CorsLayer},
    services::ServeDir,
    trace::TraceLayer,
};

use tera::Tera;

pub mod json;
pub mod routes;

use json::structures::{magmator::Magmator, reactor::Reactor};

#[derive(Clone)]
pub struct AppState {
    pub reactors: HashMap<i32, Reactor>,
    pub magmators: HashMap<i32, Magmator>,
    pub templates: Tera,
}

pub type SharedState = Arc<RwLock<AppState>>;

fn make_app() -> Router<SharedState> {
    let cors = CorsLayer::new()
        .allow_methods([Method::GET, Method::POST])
        .allow_origin(Any);

    Router::new()
        .merge(routes::routes())
        .nest_service("/assets", ServeDir::new("assets"))
        .layer(cors)
        .layer(TraceLayer::new_for_http())
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::DEBUG)
        .init();

    let templates = Tera::new("./templates/**/*").unwrap();
    let state = AppState {
        reactors: HashMap::new(),
        magmators: HashMap::new(),
        templates,
    };

    let app: Router<()> = make_app().with_state(Arc::new(RwLock::new(state)));

    let addr = "0.0.0.0:8080".parse().unwrap();
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}
